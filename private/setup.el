(require 'ox-publish)
(require 'projectile)
(setq my--exclude
      "^\\(private\\|public\\)")
(setq org-publish-project-alist
      `(("org-notes"
         :base-directory ,(projectile-project-root)
         :base-extension "org"
         :publishing-directory ,(concat (projectile-project-root) "public")
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 4             ; Just the default for this project.
         :auto-preamble t
         :exclude ,my--exclude
         :auto-sitemap t                ; Generate sitemap.org automagically...
         :sitemap-filename "sitemap.org"  ; ... call it sitemap.org (it's the default)...
         :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
         :html-preamble ""
         :html-postamble "<p class=\"date\">Website last updated: %T</p>\n")
        ("org-static"
         :base-directory ,(projectile-project-root)
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|txt\\|asc"
         :publishing-directory ,(concat (projectile-project-root) "public")
         :recursive t
         :publishing-function org-publish-attachment
         :exclude ,my--exclude
         )
        ("org" :components ("org-notes" "org-static"))))
