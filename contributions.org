#+setupfile: private/templates/level-0.org
#+TITLE: Contributions to Open Source
#+DATE: <2020-01-21 Tue>
#+options: toc:t

Goal of this document: demonstrate a history of contributing to
projects af various languages and technologies.

I make a lot of small contributions to various open source
projects. My goal is to improve my own "quality of life in computing"
with one fix at a time. As a rule of thumb, I try to use software that
I can fix much like I would try to fix any electronic or furniture I
possess.

*I do not claim ownership or any recognition beyond making important
fixes to various projects.*

Note: This is an exceprted list. It also presently does not include
most of my own projects or tickets that brought about improvements.

----------

# TODO: Add interesting issues/tickets.

* FreeBSD (2012-2018)

  I used to be loosely involved with the FreeBSD project as a ports
  maintainer. Later on I used this experience at a student job to
  write ports for a scientific computing cluster.

  https://github.com/winny-/ports

  [[https://bugs.freebsd.org/bugzilla/buglist.cgi?email1=winston&email2=Winston%2520Smith&emailassigned_to1=1&emailassigned_to2=1&emailcc1=1&emailcc2=1&emaillongdesc1=1&emaillongdesc2=1&emailreporter1=1&emailreporter2=1&emailtype1=substring&emailtype2=notsubstring&query_format=advanced][FreeBSD Bugzilla Search for my tickets]]

  (The one about lack order reversal is not posted by me.)

  [[https://github.com/outpaddling/freebsd-ports-wip/commits?author=winny-]]-

* VLC for iOS (2014)

  Add option to embolden subtitles for easier reading on low contrast
  videos.

  https://code.videolan.org/videolan/vlc-ios/commit/0544e2d40f57c516578da39f1717448d7d68556a

* Hermes (2014-2015)

  Hermes is a Pandora client for MacOS. I maintained it for a couple
  years, then gave up my hobby projects within the Apple
  Ecosystem. This project gave me new appreciation for the amount of
  hard work every software maintainer must put in to achieve even the
  most basic software quality standards.

  [[https://github.com/HermesApp/Hermes/commits?author=winny-]]

** Projects related to Hermes

   - [[https://github.com/winny-/HermesRemote][HermesRemote]] - an experimental web UI for controlling Hermes.
   - [[https://github.com/winny-/HermesSkypeControl][HermesSkypeControl]] - Play/pause Hermes when you receive a call in Skype.

* Gentoo packaging (2011, 2018-2020)

  Many years ago I made an overlay with [[https://bitbucket.org/winny-/srsbuilds/wiki/Home][some interesting ebuilds]].

  Presently I maintain an overlay of ebuilds [[https://github.com/winny-/winny-overlay][on GitHub]]. It is also on
  the list of unofficial overlays that =layman= uses.

  I still use Gentoo for various reasons; if something breaks (which
  it doesn't), it is my own fault.

* Arch Linux packaging (2017)

  I moved on from Arch Linux because of various reasons amounting to a
  system that was hard to manage software across deployments, subpar
  package writing tools (e.g. not any meaningful QA checks), and a
  community that was not pleasant to participate in.

  Never the less, I did maintain a few packages in AUR.

  - [[https://aur.archlinux.org/packages/libtcod-151/][libtcod-151]] (work around Arch's lack of slotting/parallel
    installation support)
  - [[https://aur.archlinux.org/packages/crimson/][crimson]] (Classic open source game: Crimson Fields)
  - [[https://aur.archlinux.org/packages/slashem/][slashem]] (nethack variant)
  - [[https://aur.archlinux.org/packages/marlowe/][marlowe]] (Shakespeare programming language transpiler)

* Textual (2013-2014)

  Textual is the best IRC client for MacOS. I don't use MacOS anymore,
  but I did enjoy improving it. Fixes include UX enhancements, adding
  extension AppleScripts, and reporting a bunch of issues that others
  fixed. Unfortunately the Textual issue tracker on GitHub was shut
  down some time ago. I recall finding a bug with a PING sent from the
  server would be replied by a double-PONG, causing certain IRC
  daemons to disconnect Textual clients, thereby rendering Textual not
  very usable with certain IRC networks.

  [[https://github.com/Codeux-Software/Textual/commits?author=winny-]]

  I also wrote a little [[https://github.com/winny-/Textual-DAThumbnailPlugin][DeviantArt Thumbnail plugin]] in Swift for Textual.

* applescript-json (2014)

  Yes, a JSON encoder in AppleScript. It is madness.

  [[https://github.com/mgax/applescript-json/commits?author=winny-]]


* pjson (2014-2015)

  Python based json pretty printer with color. Various fixes.

  [[https://github.com/igorgue/pjson/commits?author=winny-]]


* glacier-cli (2014)

  A simple CLI to access Amazon Web Services Glacier - a nearline
  storage service intended for storage that is not intended for online
  access.

  [[https://github.com/carlossg/glacier-cli/commits?author=winny-]]

* Homebrew (2014)

  A couple package bumps in the de-facto Mac OS X package manager.

  [[https://github.com/Homebrew/legacy-homebrew/commits?author=winny-]]

* mcstatus (2015)

  That Python package written by dinnerbone (a minecraft dev) to ping
  minecraft servers. I fixed up the packaging to Pythonista
  expectations.

  [[https://github.com/Dinnerbone/mcstatus/commits?author=winny-]]

* xbanish (2015)

  Tool to hide mouse. Resolved some packaging quirks.

  [[https://github.com/jcs/xbanish/commits?author=winny-]]

* cyberpunk-theme.el (2018-2019)

  Best theme for emacs. UX improvements.

  [[https://github.com/jcs/xbanish/commits?author=winny-]]


* yossarian-bot (2015-2019)

  IRC bot. Mostly adding plugins and dockerizing.

  [[https://github.com/woodruffw/yossarian-bot/commits?author=winny-]]


* racket-irc (2015-2018)

  IRC client library for Racket. Added TLS support.

  [[https://github.com/woodruffw/yossarian-bot/commits?author=winny-]]

* Nimdok (2014-2015)

  IRC bot. Tidy up and add some plugins.

  [[https://github.com/woodruffw/yossarian-bot/commits?author=winny-]]

* pycodestyle (2014)

  Popular python code linter. Formerly called pep8.

  [[https://github.com/PyCQA/pycodestyle/commits?author=winny-]]

* mibot (2017)

  IRC Bot. Minor fix.

  [[https://github.com/Nyubis/mibot/commits?author=winny-]]


* Qutebrowser (2018)

  Good keyboard-first web browser. Documentation tweaks.

  [[https://github.com/qutebrowser/qutebrowser/commits?author=winny-]]


* slop (2018)

  Select rectangles, windows, or screens for screenshots and other
  tasks.

  Document some things, fix a segfault.

  [[https://github.com/naelstrof/slop/commits?author=winny-]]


* cava (2018)

  Text based music visualizer.

  Document some keyboard shortcuts.

  [[https://github.com/karlstav/cava/commits?author=winny-]]


* emacs-dashboard (2018)

  Landing page for your emacs.

  Document some things. Check back for some QoL fixes.

  [[https://github.com/emacs-dashboard/emacs-dashboard/commits?author=winny-]]

* cargo-ebuild (2018)

  Tool to generate .ebuild files (for packaging software on gentoo)
  from Cargo crates (used for the Rust programming language).

  Fix a link.

  [[https://github.com/cardoe/cargo-ebuild/commits?author=winny-]]


* org-static-blog (2019)

  Static website generator.

  Fix to RSS feed generation. Add way to force complete regeneration
  of the website.

  [[https://github.com/bastibe/org-static-blog/commits?author=winny-]]


* typed-racket (2019)

  Typed variant of Racket.

  Add some more types to untyped racket bits.

  [[https://github.com/racket/typed-racket/commits?author=winny-]]


* the racket website (2019)

  Every site needs a favicon.

  [[https://github.com/racket/racket-lang-org/commits?author=winny-]]


* racket (2018-2019)

  Documentation tweaks. Fix/report issue with GCC/GNU Make detection
  in 7.4.

  [[https://github.com/racket/racket/commits?author=winny-]]

* Retroarch (2020)

  [[https://github.com/libretro/docs/commits?author=winny-][Improve some documentation]].

* Alpine (2020)

  [[https://github.com/alpinelinux/aports/commits?author=winny-][Contributing multiple improvements]] to the Alpine Ports project used to build
  the packages for Alpine Linux.

  P.S. Alpine is pretty nice :)
* [[https://en.wikipedia.org/wiki/Mdadm][mdadm]] (2020)
  Just [[https://git.kernel.org/pub/scm/utils/mdadm/mdadm.git/commit/?id=5e592e1ed809b94670872b7a4629317fc1c8a5c1][a tiny patch]] to fix the documentation.
