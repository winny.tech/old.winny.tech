
| Email:    | [[mailto:hello@winny.tech][=hello@winny.tech=]]             |
| Homepage: | [[https://winny.tech/][=https://winny.tech/=]]              |
| GitHub:   | [[https://github.com/winny-/][=https://github.com/winny-=]] |

* Experience
  :PROPERTIES:
  :CUSTOM_ID: experience
  :CLASS: unnumbered
  :END:

*Site Reliability Engineer*, Kohl's, Inc.; Remote --- June
2021--April 2022. Collaborate across multiple teams ensuring software
reliability while adding new features. Support financial users and
develop features for Business Intelligence (BI), ETL, monitoring. Java,
Spring, Python, Ansible.

*Software Developer*, Roydan Enterprises LLC.; Manitowoc, WI --- Sept
2020--June 2021. Legacy software maintenance with OpenEdge ABL and C#
.NET. Code review and team work.

*Software & IT Freelance Consulting*; Remote --- 2014--2019.
Responsibilities include project management, security auditing, system
administration.

*Research Assistant*, University of Wisconsin-Milwaukee; Milwaukee, WI
--- 2017. Responsibilities include writing pkgsrc packages, assisting
users of High Performance Computing cluster.

*Computer Science Undergrad Grader*, University of Wisconsin-Milwaukee;
Milwaukee, WI --- 2017. Responsibilities include grading Python
programming assignments and grading exams.

* Education
  :PROPERTIES:
  :CUSTOM_ID: education
  :CLASS: unnumbered
  :END:

University of Wisconsin, Manitowoc, WI --- B.S. Computer Science,
2014--2016

University of Wisconsin-Milwaukee, Milwaukee, WI --- B.S. Computer
Science, 2016--Graduated Spring 2020.

* Skills
  :PROPERTIES:
  :CUSTOM_ID: skills
  :CLASS: unnumbered
  :END:

Software development in Python, C++, Racket (Scheme), Java, PHP,
Assembly, Scala, JS, OpenEdge ABL, C# .NET

Experience with Software Project Management as lead developer and
project manager

Configuration management of Linux and FreeBSD servers using Ansible

Git, Mercurial, and Subversion version control systems

Packaging software for Gentoo, FreeBSD, Archlinux, Alpine Linux, and Mac
Homebrew

Teaching & mentoring technical topics including beginner programming and
computer science theory

Computer Security auditing with emphasis with server applications.
Reported critical vulnerabilities.

Docker containers and OpenVZ, OpenShift & K8s

Server software management including GitLab, Rails, Wordpress, mongoDB,
PostgreSQL, MySQL

Cloud Management of Amazon AWS, Oracle Cloud, GCP

* Activities
  :PROPERTIES:
  :CUSTOM_ID: activities
  :CLASS: unnumbered
  :END:

*Board Member*, McKinley Academy; Manitowoc, WI --- Spring 2021--Spring
2022

*President of IEEE-CS @ UWM*, University of Wisconsin-Milwaukee;
Milwaukee, WI --- Fall 2019--Spring 2020

*Officer of IEEE-CS @ UWM*, University of Wisconsin-Milwaukee;
Milwaukee, WI --- Fall 2017--Spring 2019

*Active in the open source community* --- contributions to VLC, FreeBSD,
PEP8, Racket and countless others


